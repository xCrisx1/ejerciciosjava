import javax.swing.JOptionPane;

public class Ejercicio10 {
	//Principal (la he llamado ejercicio10 para ordenar mas todo)
	public static void main(String[] args) {
		//Crear un programa usando la clase Diccionario, la Clase usuario, la clase Principal.
		//Pedirle al usuario su nombre y apellido. Crear un objeto a partir de esos datos ingresados, vamos a
		//preguntarle si quiere ver la capital de algun pais o ver la descripcion de alguna ciudad y le vamos
		//a ofrecer esa funcionalidad a travez de 
		//JOptionPane.showInputDialog("message");
		//JOptionPane.showMessageDialog(null,"message");
		//crear el codigo en la clase principal y llamar a la clase usuario y la clase Diccionario desde la clase principal
		
		Usuario2 user = new Usuario2(JOptionPane.showInputDialog("Ingresa tu Nombre"), JOptionPane.showInputDialog("Ingresa tu Apellido"));
		JOptionPane.showMessageDialog(null, "Bienvenido " + user.Nombre + " " + user.Apellido);
		
		Diccionario2 dic = new Diccionario2();
		
		while(dic.seguirejecutando == true) {
		String Opcion = JOptionPane.showInputDialog("�Desea ver la capital de algun pais o ver la descripcion de alguna ciudad de Chile? (escribir capital o ciudad)");
		if( Opcion.equals("capital")) {
			JOptionPane.showMessageDialog(null, dic.buscarCapital(JOptionPane.showInputDialog("Ingrese el pais del cual desea saber su capital")));
		}else if(Opcion.equals("ciudad"))
		{
			JOptionPane.showMessageDialog(null, dic.buscarCiudad(JOptionPane.showInputDialog("Ingrese una ciudad de Chile de la cual desea saber su descripcion")));
		}else { 
			JOptionPane.showMessageDialog(null,"No se ha reconocido la respuesta");
		}
		}
	}

}
