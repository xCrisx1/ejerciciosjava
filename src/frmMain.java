import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class frmMain {

	private JFrame frame;
	private JTextField txtSuma1;
	private JTextField txtSuma2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frmMain window = new frmMain();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public frmMain() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 297, 197);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblResultado = new JLabel("Resultado=");
		lblResultado.setHorizontalAlignment(SwingConstants.CENTER);
		lblResultado.setBounds(86, 69, 116, 14);
		frame.getContentPane().add(lblResultado);
		
		JLabel label = new JLabel("+");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(117, 14, 46, 14);
		frame.getContentPane().add(label);
		
		txtSuma1 = new JTextField();
		txtSuma1.setBounds(21, 11, 86, 20);
		frame.getContentPane().add(txtSuma1);
		txtSuma1.setColumns(10);
		
		txtSuma2 = new JTextField();
		txtSuma2.setBounds(176, 14, 86, 20);
		frame.getContentPane().add(txtSuma2);
		txtSuma2.setColumns(10);
		
		JButton btnMostrar = new JButton("Mostrar");
		btnMostrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//declaramos variables
				int Num1, Num2, Suma;
				//le asignamos los valores transformandolos a numeros enteros
				Num1 = Integer.parseInt(txtSuma1.getText());
				Num2 = Integer.parseInt(txtSuma2.getText());
				//sumamos las variables
				Suma = Num1 + Num2;
				//pasamos de valor entero a String (cadena de texto)
				lblResultado.setText(Integer.toString(Suma));
			}
		});
		btnMostrar.setBounds(98, 39, 89, 23);
		frame.getContentPane().add(btnMostrar);
	}
}
