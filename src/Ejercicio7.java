import java.awt.TrayIcon.MessageType;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Ejercicio7 {

	public static void main(String[] args) {
		//ingresar 6 numeros por pantalla, 3 enteros y 3 decimales
		//los enteros almacenarlos en un arreglo int y los decimales en un arreglo de tipo double
		Scanner sc = new Scanner(System.in);
		//JOptionPane.showMessageDialog(null, "asd");;
		
		int[] enteros = new int[3];
		double[] decimales = new double[3];
		
		String contenedor = "";
		
		for(int i = 0;i <3;i++) {
			contenedor = JOptionPane.showInputDialog("Ingresa el entero" + (i + 1));
			if (contenedor == null) contenedor = "0";
			enteros[i] = Integer.parseInt(contenedor);
		}
		for(int i = 0;i <3;i++) {
			contenedor = JOptionPane.showInputDialog("Ingresa el decimal" + (i + 1));
			if (contenedor == null) contenedor = "0";
			decimales[i] = Double.parseDouble(contenedor);
		}
		
		for(int i = 0;i <3;i++) {
			JOptionPane.showMessageDialog(null, "entero numero " + (i + 1) + ": " + enteros[i]);
		}
		
		for(int i = 0;i <3;i++) {
			JOptionPane.showMessageDialog(null, "decimal numero " + (i + 1) + ": " + decimales[i]);
		}
		
	}

}
