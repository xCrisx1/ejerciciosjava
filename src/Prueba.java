import java.util.Scanner;

public class Prueba {

	public static void main(String[] args) {
		//Viendo variables int, String, etc.
		//los short son como el int en vb y los int son como long en vb, mientras que long tiene mucho mas alcance, el double es diferente
		//el float para poder declararse se usa asi:
		float numero = (float) 89.56;
		//al igual que short
		short numero2 = (short) 44541;
		
		//para declarar char, debe ir con una sola comilla ' y solo contiene un caracter
		char a = 'a';
		
		//Dato no primitivo Integer
		Integer numerox = 10;
		
		Scanner sc = new Scanner(System.in);
		
		String nombre = sc.nextLine(); //tambien se puede usar para numeros con sc.nextInt
		
		System.out.println(nombre);
		//System.out.println("a \n s \t d");
		//System.out.println("\" asd");
		//System.out.printf("el numero %d sera remplazado por la variable numero1 y despues este %d sera sustituido por numero2", numero1, numero2);
		
	}

}
