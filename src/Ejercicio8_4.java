import java.util.Scanner;

import javax.swing.JOptionPane;

public class Ejercicio8_4 {

	public static void main(String[] args) {
		//Ejemplo de buscador de ciudades
		//al utilizar JOptionPane, solo se puede utilizar una comparacion con la funcion equals, ya que == no lo toma como
		//String, lo toma como objeto
				
		String[][] ciudades = {
		{"valdivia", "talca"},
		{"santiago", "concepcion"},
		{"puerto montt", "valparaiso"},
		{"antofagasta", "iquique"}
		};
		
		boolean encontrado = false;
		String buscador = JOptionPane.showInputDialog("ingrese la palabra a buscar");
		
		for(int i = 0;i < ciudades.length; i++) {
			if(ciudades[i][0].equals(buscador)) encontrado = true;
			if(ciudades[i][1].equals(buscador)) encontrado = true;
		}
		
		if(encontrado) {
			JOptionPane.showMessageDialog(null, "Encontrado");
		}else {
			JOptionPane.showMessageDialog(null, "No se han encontrado coincidencias");
		}

	}

}
