
public class Ejercicio6 {

	public static void main(String[] args) {
		//mostrar numeros de 0 al 100 de 7 en 7
		int contador = 0;
		
		do{
			contador += 7;
			
			if(contador <= 100) System.out.println(contador);
			
		}while(contador <= 100);
	}

}
