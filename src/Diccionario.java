import javax.swing.JOptionPane;

public class Diccionario {
	
	public String ciudades = "roma, paris... etc";
	public String[][] capitales = {
			{"Chile", "Santiago"},
			{"Venezuela","Caracas"},
			{"Peru","Lima"}
	};
	
	private int anioPublicacion = 1996;
	
	public String retornaFecha() {
		return "15 de Mayo de " + anioPublicacion;
	}
	
	public String buscarCiudades(String pais) {
		for(int i = 0;i < capitales.length; i++) {
			if(capitales[i][0].equals(pais)) return "la capital de " + pais + " es " + capitales[i][1];
		} 
		
		return "no encontrado";
	}
	
	public void Ejercicio8() {
		String[][] ciudades = {
			{"", ""},
			{"", ""},
			{"", ""},
			{"", ""}
		};
		
		for(int i = 0;i < ciudades.length; i++) {
			ciudades[i][0] = JOptionPane.showInputDialog("introduzca el nombre de la ciudad " + (i + 1)); 
			if(ciudades[i][0] == null) ciudades[i][0] = "Ninguna";
			ciudades[i][1] = JOptionPane.showInputDialog("introduzca la descripcion de la ciudad");
			if (ciudades[i][1] == null) ciudades[i][1] = "No hay descripcion";
		}
		for(int i = 0;i < ciudades.length; i++) {
			JOptionPane.showMessageDialog(null, ciudades[i][0] + ": " + ciudades[i][1]);
		}
	}
}
