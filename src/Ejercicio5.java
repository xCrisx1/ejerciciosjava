import java.util.Scanner;

public class Ejercicio5 {

	public static void main(String[] args) {
		//hacer en el mismo ejercicio4, otra funcion, llamando cualquiera de las dos con un switch.
		Scanner sc = new Scanner(System.in);
		Ejercicio5 Ejercicio = new Ejercicio5();
		int opcion = 0;
		boolean preguntar = true;
		
		while(preguntar == true) {
			System.out.println("Seleccione una de las siguientes opciones \n1.Primer Ejercicio \n2.Segundo Ejercicio");
			opcion = sc.nextInt();
			switch (opcion) {
			case 1:
				Ejercicio.PrimerEjercicio();
				preguntar = false;
				break;
			case 2:
				Ejercicio.SegundoEjercicio();
				preguntar = false;
				break;
			}
		}
		
	}
	public void PrimerEjercicio() {
		//Leer un numero e indicar si es positivo o negativo. El proceso se repetira hasta que se introduza un 0
		//**mostrar cuantos numeros se han introducido
		//**acumular el producto de todos los numeros ingresados
		//**acumular el residuo de todos los numeros ingresados, partiendo del primer numero ingresado, haciendo una
		//division del primer numero ingresado con el siguente
		//double acumulador %= acumular;zz
		
		System.out.println("Has Seleccionado el Primer Ejercicio");
		
		int numero = 0;
		int contador = 0;
		long multiplicador = 0;
		int divisor = 0;
				
		Scanner sc = new Scanner(System.in);
				
		while(true) {
			System.out.println("Introduzca un numero (si introduce 0 el programa finalizara)");
			numero = sc.nextInt();
					
			if (numero > 0) {
				System.out.println("el numero es positivo");
			}else if(numero < 0) {
				System.out.println("el numero es negativo");
			}else {
				break;
			}
					
			contador++;
			if(multiplicador != 0) {
				multiplicador *= numero;
			}else {
				multiplicador = numero;
			}
					
			if (divisor != 0) {
				divisor %= numero;
			}else {
				divisor = numero;
			}
					
		} 
		System.out.println("Se han ingresado: " + contador + " numeros.");
		System.out.println("El producto de todos los numeros es de: " + multiplicador);
		System.out.println("El residuo de todos los numeros es de: " + divisor);
	}
	
	public void SegundoEjercicio() {
		System.out.println("Has seleccionado el Segundo Ejercicio");
		//Pedir numeros hasta que se teclee uno negatio, y mostrar cuantos numeros se han introducido
		
		Scanner sc = new Scanner(System.in);
		int numero = 0;
		int contador = 0;
		
		while(true) {
			System.out.println("Ingrese un numero (cuando ingrese un numero negativo, el programa finalizara)");
			numero = sc.nextInt();
			
			if(numero < 0)break;
			contador ++;
		}
		
		System.out.println("Se han introducido " + contador + " numeros");
	}

}
