
public class Ejercicio9 {

	public static void main(String[] args) {
		//hacer una clase donde poder asignarle valores tanto estaticos como solo publicos
		
		Usuario primerUsuario = new Usuario("Casta�os");
		Usuario segundoUsuario = new Usuario("Verdes");
		
		primerUsuario.Edad = 18;
		segundoUsuario.Edad = 20;
		
		primerUsuario.Raza = "Ninguna";
		segundoUsuario.Raza = "Ninguna";
		
		primerUsuario.Nombre = "Xavier";
		segundoUsuario.Nombre = "Felipe";
		
		Usuario.Raza = "Humano";
		
		System.out.println("Nombre:" + primerUsuario.Nombre + ", Edad:" + primerUsuario.Edad + ", Color Ojos:" + primerUsuario.ojos + ", Raza:" + primerUsuario.Raza);
		System.out.println("Nombre:" + segundoUsuario.Nombre + ", Edad:" + segundoUsuario.Edad + ", Color Ojos:" + segundoUsuario.ojos + ", Raza:" + segundoUsuario.Raza);
	}

}
