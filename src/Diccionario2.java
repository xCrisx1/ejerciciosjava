
public class Diccionario2 {
	public boolean seguirejecutando = true;
	String[][] capitales = {
			{"CHILE","Santiago"},
			{"COLOMBIA","Bogot�"},
			{"PERU","Lima"},
			{"ESPA�A","Madrid"},
			{"VENEZUELA","Caracas"},
	};
	
	String[][] ciudades = {
			{"VALDIVIA", "ciudad lluviosa"},
			{"SANTIAGO", "ciudad central"},
			{"PUERTO MONTT", "ciudad sidosa"},
			{"ANTOFAGASTA", "ciudad calurosa"}
	};
	
	public String buscarCapital(String pais) {
		for(int i = 0; i < capitales.length;i++) {
			if (capitales[i][0].equals(pais.toUpperCase())) {
				seguirejecutando = false;
				return "La capital de " + pais + " es " + capitales[i][1];
			}
		}
		return "no se encontro la capital";
	}
	
	public String buscarCiudad(String ciudad) {
		for(int i = 0; i < ciudades.length;i++) {
			if (ciudades[i][0].equals(ciudad.toUpperCase())) {
				seguirejecutando = false;
				return ciudad + " es " + ciudades[i][1];
			}
		}
		
		return "no se encontro descripcion para la ciudad ingresada";
	}
}
