import java.util.Scanner;

public class Ejercicio1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int val = 1;
		int i;
		String paresS = "";
		String imparesS = "";
		
		long pares = 0;
		double impares = 0;
		
		System.out.println("Ingrese el maximo de numeros a ingresar");
		int maxNum = sc.nextInt();
		
		for (i = 0; i <= maxNum && val != 0 ; i++) {
			System.out.println("Ingresa un valor \n(si desea dejar de ingresar numeros escriba 0)");
			val = sc.nextInt();
			
			i += 1;
			
			if (val != 0) {
				if ((val % 2) == 0) {
					
					if(pares == 0) {
						pares = val;
					}else {
						pares *= val;
					}
					
					paresS += Integer.toString(val) + ", ";
				}else {
					if (impares == 0) {
						impares = val;
					}else {
						impares /=  val;
					}
					
					imparesS += Integer.toString(val) + ", ";
				} 
			}
			
		}//fin ciclo For
		
		System.out.println("los numeros pares son: " + paresS);
		System.out.println("y la multiplicacion entre ellos nos da un total de " + pares);
		
		System.out.println("los numeros impares son: " + imparesS);
		System.out.println("y la division entre ellos nos da un total de " + impares);
	}

}
