import java.util.Scanner;

import javax.swing.JOptionPane;

public class Ejercicio8_3 {

	public static void main(String[] args) {
		//Declarar un Arreglo de tipo string con ciudades y una descripcion para cada una de esas ciudades. Recorrer arreglo
		//dentro de un for y en funcion de la ciudad vamos a retornar la descripcion
		
		String[][] ciudades = {
		{"", ""},
		{"", ""},
		{"", ""},
		{"", ""}
		};
		
		
		for(int i = 0;i < ciudades.length; i++) {
			ciudades[i][0] = JOptionPane.showInputDialog("introduzca el nombre de la ciudad " + (i + 1));
			if(ciudades[i][0] == null) ciudades[i][0] = "Ninguna";
			ciudades[i][1] = JOptionPane.showInputDialog("introduzca la descripcion de la ciudad");
			if (ciudades[i][1] == null) ciudades[i][1] = "No hay descripcion";
			
		}
		
		for(int i = 0;i < ciudades.length; i++) {
			JOptionPane.showMessageDialog(null, ciudades[i][0] + ": " + ciudades[i][1]);
		}
	}

}
