
public class Otros {

	public static void main(String[] args) {
		//Ambas operaciones son iguales
		int edad1 = 10;
		int edad2 = 12;
		
		if(edad1 >edad2) {
			int valor = 1;
		}else {
			int valor = 2;
		}
		
		int valor = (edad1 > edad2)?1:2; //este se utiliza para ambos casos
		
		if(edad1 > edad2) valor=1; //este if es de una sola linea, pero no tiene else
		//fin operaciones
	}

}
