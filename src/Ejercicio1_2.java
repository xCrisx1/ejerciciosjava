import java.util.Scanner;

public class Ejercicio1_2 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int resultado;
		
		System.out.println("Ingrese el primer numero");
		int num1 = sc.nextInt();
		
		System.out.println("Ingrese el segundo numero");
		int num2 = sc.nextInt();
		
		if(num1 % 2 == 0 && num2 % 2 == 0) {
			resultado = num1 * num2;
			System.out.println("Los numeros son pares, y su multiplicacion es: " + resultado);
		}else if(num1 % 2 > 0 && num2 % 2 > 0) {
			resultado = num1 / num2;
			System.out.println("Los numeros son impares, y su division es: " + resultado);
		}else {
			System.out.println("Se ha ingresado un numero par y uno impar por ende quedan iguales: " + num1 + ", " + num2);
		}
		
	}

}
