
public class Ejercicio8_2 {

	public static void main(String[] args) {
		//Declarar un Arreglo de tipo string con ciudades y una descripcion para cada una de esas ciudades. Recorrer arreglo
		//dentro de un for y en funcion de la ciudad vamos a retornar la descripcion
		
		String[][] ciudades = {
				{"valdivia", "ciudad lluviosa"},
				{"santiago", "ciudad central"},
				{"puerto montt", "ciudad sidosa"},
				{"antofagasta", "ciudad calurosa"}
		};
		
		for(int i = 0;i < ciudades.length; i++) {
				System.out.println(ciudades[i][0] + ": " + ciudades[i][1]);  
		}
	}

}
