import java.util.Scanner;

public class Test2 {

	public static void main(String[] args) {
		int contador = 0;
		//EN UN BUCLE TAMBN SE PUEDE USAR EL continue; :D
		
		//contador = ++contador;
		//contador = contador++;
		//contador ++;
		//System.out.println(contador);
		Scanner sc = new Scanner(System.in);
		int numero = 0;
		double decimal = 0;
		
		numero = sc.nextInt();
		decimal = sc.nextDouble();
		
		System.out.println(numero);
		System.out.println(decimal);
		
		//el decimal en el scanner solo lo acepta con ","
		//por ejemplo: 15,3 
	}

}
