import java.util.Scanner;

public class Ejercicio4 {

	public static void main(String[] args) {
		//Leer un numero e indicar si es positivo o negativo. El proceso se repetira hasta que se introduza un 0
		//**mostrar cuantos numeros se han introducido
		//**acumular el producto de todos los numeros ingresados
		//**acumular el residuo de todos los numeros ingresados, partiendo del primer numero ingresado, haciendo una
		//division del primer numero ingresado con el siguente
		//double acumulador %= acumular;zz
		
		int numero = 0;
		int contador = 0;
		long multiplicador = 0;
		int divisor = 0;
		
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			System.out.println("Introduzca un numero (si introduce 0 el programa finalizara)");
			numero = sc.nextInt();
			
			if (numero > 0) {
				System.out.println("el numero es positivo");
			}else if(numero < 0) {
				System.out.println("el numero es negativo");
			}else {
				break;
			}
			
			contador++;
			if(multiplicador != 0) {
				multiplicador *= numero;
			}else {
				multiplicador = numero;
			}
			
			if (divisor != 0) {
				divisor %= numero;
			}else {
				divisor = numero;
			}
			
		} 
		System.out.println("Se han ingresado: " + contador + " numeros.");
		System.out.println("El producto de todos los numeros es de: " + multiplicador);
		System.out.println("El residuo de todos los numeros es de: " + divisor);
	}

}
