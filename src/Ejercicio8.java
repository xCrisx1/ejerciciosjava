
public class Ejercicio8 {

	public static void main(String[] args) {
		//Declarar un Arreglo de tipo string con ciudades y una descripcion para cada una de esas ciudades. Recorrer arreglo
		//dentro de un for y en funcion de la ciudad vamos a retornar la descripcion
		String[][] ciudades = new String[4][2];
		
		ciudades[0][0] = "valdivia";
		ciudades[0][1] = "ciudad lluviosa";
		ciudades[1][0] = "santiago";
		ciudades[1][1] = "ciudad central";
		ciudades[2][0] = "puerto montt";
		ciudades[2][1] = "ciudad sidosa";
		ciudades[3][0] = "antofagasta";
		ciudades[3][1] = "ciudad calurosa";
		
		for(int i = 0;i < 4; i++) {
			for (int j = 0; j < 2; j++) {
				System.out.println(ciudades[i][j]);
			}
		}
	}

}
