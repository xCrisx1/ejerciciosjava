import java.util.Scanner;

public class Ejercicio2 {

	public static void main(String[] args) {
		//En una tienda se aplica un 20% de descuento a ni�os, 10% para adultos. Tomando en cuenta que se considera
		//ni�o a aquellos que tienen una edad comprendida entre 0 - 17 a�os
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Ingresa tu edad");
		int edad = sc.nextInt();
		
		System.out.println("Ingresa el valor del producto");;
		int valor = sc.nextInt();
		
		if (edad < 18) {
			System.out.println("Tienes el descuento aplicado a ni�os, el cual equivale al 20% \nEl valor final de $" + valor + " sera de $" + Math.round(valor - (valor * 0.20)) );
		}else {
			System.out.println("Tienes el descuento aplicado a adultos, el cual equivale al 10% \nEl valor final de $" + valor + " sera de $" + Math.round(valor - (valor * 0.10)) );
		}
		
	}

}
